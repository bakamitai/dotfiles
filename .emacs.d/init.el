;;; -*- lexical-binding: t -*-
(setq og-gc-cons-threshold gc-cons-threshold)
(setq gc-cons-threshold (* 1024 1024 128))
;; Cringe GUI elements BEGONE
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(add-hook 'after-init-hook (lambda () (setq gc-cons-threshold og-gc-cons-threshold) (message "Startup done babe!! %f" gc-elapsed)))

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(use-package tree-sitter :ensure t :defer t)
(use-package tree-sitter-langs :ensure t :defer t)

(use-package tree-sitter-hl
  :hook (rust-mode c-mode python-mode js-mode))

(use-package gcmh
  :disabled t
  :ensure t
  :pin gnu
  :hook (after-init . gcmh-mode))

(use-package orderless
  :ensure t
  :pin gnu
  :custom (completion-styles '(orderless basic)))

(use-package gdb-mi
  :custom (gdb-many-windows t))

(use-package pdf-tools
  :ensure t
  :pin nongnu
  :mode ("\\.pdf\\'" . pdf-view-mode))

(use-package nodejs-repl
  :ensure t
  :commands nodejs-repl)

(use-package nov
  :ensure t
  :mode ("\\.epub\\'" . nov-mode))

(use-package minions
  :ensure t
  :custom
  (minions-mode-line-lighter "minor-modes")
  (minions-prominent-modes '(meow-normal-mode meow-insert-mode meow-motion-mode))
  :config (minions-mode 1))

(use-package tmr
  :ensure t
  :pin gnu
  :commands tmr)

(use-package org
  :custom
  (org-directory "~/org/")
  (org-agenda-files `(,(concat org-directory "todo.org")))
  (org-default-notes-file (concat org-directory "notes.org"))
  (org-capture-templates '(("r" "Reference" entry (file "")
                            "* %^{prompt}\n%t\n%?\n%a")
                           ("j" "Journal" entry (file "journal.org")
                            "* %t\n%?")
                           ("t" "Todo" entry (file "todo.org")
                            "* TODO %?\n%^t")
                           ("p" "Project" entry (file "projects.org")
                            "* TODO %?")))
  :bind (("C-c o l" . org-store-link)
         ("C-c o a" . org-agenda)
         ("C-c o c" . org-capture)
         :repeat-map org-heading-repeat-map
         ("n" . org-next-visible-heading)
         ("p" . org-previous-visible-heading))
  :hook (org-mode . variable-pitch-mode))

(use-package vterm
  :ensure t
  :commands vterm
  :custom (vterm-max-scrollback 100000))

(use-package esh-mode
  :bind (:map eshell-mode-map
              ("C-r" . cape-history)))

(use-package comint
  :bind (:map comint-mode-map
              ("C-r" . cape-history)))

(use-package sxhkdrc-mode
  :ensure t
  :pin gnu
  :mode "sxhkdrc\\'")

(use-package slime
  :ensure t
  :pin nongnu
  :commands slime
  :custom (inferior-lisp-program "/usr/bin/sbcl"))

(use-package rust-mode
  :ensure t
  :pin nongnu
  :mode "\\.rs\\'")

(use-package zig-mode
  :ensure t
  :pin nongnu
  :mode "\\.zig\\'")

(use-package eglot
  :preface
  (defvar-keymap eglot-prefix-map
  :doc "My prefix map for eglot"
  "a" 'eglot-code-actions
  "r" 'eglot-rename)
  :config
  (add-to-list 'eglot-server-programs
               '((rust-ts-mode rust-mode) .
                 ("~/.cargo/bin/rust-analyzer" :initializationOptions (:check (:command "clippy")))))
  (fset #'jsonrpc--log-event #'ignore)
  ;; :bind (("C-c e r" . eglot-rename)
  ;;        ("C-c e a" . eglot-code-actions))
  :hook (((c-mode rust-mode zig-mode) . eglot-ensure)))

(use-package eldoc
  :custom (eldoc-echo-area-use-multiline-p 2))

(use-package cape
  :ensure t
  :pin gnu
  :preface
  (defun add-cape-file ()
      (add-to-list 'completion-at-point-functions #'cape-file))

  :custom
  (cape-file-directory-must-exist t)
  (cape-dabbrev-min-length 3)

  :hook ((prog-mode . add-cape-file)
         (eglot-managed-mode . add-cape-file))

  :init
  (dolist (backend '(cape-dabbrev cape-keyword cape-file))
    (add-to-list 'completion-at-point-functions backend)))

(use-package python
  :mode ("\\.py\\'" . python-mode)
  ;; Python completion doesn't work for some reason and I don't care enough to fix it so rely on good old fashioned dabbrev baby
  :hook (python-mode . (lambda ()
                         (setq-local completion-at-point-functions
                                     '(#'cape-file #'cape-keyword #'cape-dabbrev)))))

(use-package compile
  :defer t
  :config (add-to-list 'compilation-finish-functions
                       (lambda (buffer message)
                         (select-window (get-buffer-window buffer)))))

(use-package window
  :custom (display-buffer-alist
           '(("\\*compilation\\*"
              (display-buffer-reuse-mode-window
               display-buffer-below-selected)
              (dedicated . t)
              (window-height . 0.2))
             ("\\*tex-shell\\*"
              (display-buffer-reuse-mode-window
               display-buffer-below-selected)
              (dedicated . t)
              (window-height . 0.2))
             ("\\*eldoc\\*"
              (display-buffer-reuse-mode-window
               display-buffer-below-selected)
              (dedicated . t)
              (window-height . fit-window-to-buffer)))))

(use-package recentf
  :custom (recentf-max-saved-items 50)
  :config (recentf-mode 1))

(use-package vertico
  :ensure t
  :pin gnu
  :config (vertico-mode 1))

(use-package corfu
  :ensure t
  :pin gnu
  :config (global-corfu-mode 1))

(use-package consult
  :ensure t
  :pin gnu
  :preface
  (defvar consult--dotfiles-list nil
    "List of config files used as a `consult-buffer' source.")

  (defun consult--dotfiles (&optional refresh)
    "Generate and cache list of config files.
Used as a `consult-buffer' source. If REFRESH is non nil generate the list
again."
    (interactive "p")
    (when (or (not consult--dotfiles-list) refresh)
      (setq consult--dotfiles-list
            (string-split
             (shell-command-to-string "find ~/dotfiles -name \".git\" -prune -o -type f -print")
             "\n")))
    consult--dotfiles-list)

  (defvar consult--source-dotfile
    `(:name "Dotfile"
            :narrow ?d
            :category file
            :hidden t
            :items ,#'consult--dotfiles
            :action ,#'find-file))
  :config
  (add-to-list 'consult-buffer-sources 'consult--source-dotfile 'append)

  (defvar-keymap consult-prefix-map
    :doc "Prefix map for consult commands"
    "l" #'consult-line
    "m" #'consult-man
    "i" #'consult-imenu
    "f" #'consult-find
    "g" #'consult-grep
    "n" #'consult-info
    "b" #'consult-bookmark)

  (keymap-set global-map "C-c s" consult-prefix-map)

  :custom (consult-preview-key nil)
  :bind (("C-c '" . consult-buffer)
         ;; ("C-c s" . consult-prefix-map)
         ;; ("C-c s l" . consult-line)
         ;; ("C-c s m" . consult-man)
         ;; ("C-c s i" . consult-imenu)
         ;; ("C-c s f" . consult-find)
         ;; ("C-c s g" . consult-grep)
         ;; ("C-c s n" . consult-info)
         ;; ("C-c s b" . consult-bookmark)
         ))

(use-package embark
  :ensure t
  :pin gnu
  :config
  (setf (alist-get 'file embark-default-action-overrides) #'find-file-other-window)
  (setf (alist-get 'buffer embark-default-action-overrides) #'switch-to-buffer-other-window)
  :bind (:map minibuffer-mode-map
              ("C-." . embark-act)
              ("M-o" . embark-dwim)))

(use-package embark-consult
  :ensure t
  :pin gnu
  :after (consult embark))

(use-package dired
  :custom (dired-listing-switches "-AXGlh --group-directories-first")
  :hook (dired-mode . dired-hide-details-mode))

(use-package ef-themes
  :ensure t
  :pin gnu
  :preface
  (defun setup-theme-and-fonts ()
    (load-theme 'ef-melissa-dark t)
    (set-face-attribute 'default nil :font "Hack-13")
    (set-face-attribute 'fixed-pitch nil :font "Hack-13")
    (set-face-attribute 'variable-pitch nil :font "FiraGO-14"))
  :custom
  (ef-themes-mixed-fonts t)
  (ef-themes-variable-pitch-ui t)
  (ef-themes-to-toggle '(ef-melissa-light ef-melissa-dark))
  (ef-themes-headings
   '((1 light variable-pitch 1.5)
     (2 regular 1.3)
     (3 1.1)
     (agenda-date 1.3)
     (agenda-structure variable-pitch light 1.8)
     (t variable-pitch)))
  :hook (after-init . setup-theme-and-fonts))

(use-package savehist
  :config (savehist-mode 1))

(use-package meow
  :ensure t
  :preface
  (defun luke/delete-window-or-kill-buffer ()
    (interactive)
    (if (> (count-windows) 1)
        (delete-window)
      (kill-this-buffer)))

  (defun my/meow-setup ()
    (meow-motion-overwrite-define-key
     ;; Use e to move up, n to move down.
     ;; Since special modes usually use n to move down, we only overwrite e here.
     '("u" . meow-prev)
     '("e" . meow-next)
     '("<escape>" . ignore))

    (meow-leader-define-key
     '("?" . meow-cheatsheet)
     ;; To execute the originally e or u in MOTION state, use SPC e or SPC u.
     '("u" . "H-u")
     '("e" . "H-e")
     '("1" . meow-digit-argument)
     '("2" . meow-digit-argument)
     '("3" . meow-digit-argument)
     '("4" . meow-digit-argument)
     '("5" . meow-digit-argument)
     '("6" . meow-digit-argument)
     '("7" . meow-digit-argument)
     '("8" . meow-digit-argument)
     '("9" . meow-digit-argument)
     '("0" . meow-digit-argument))

    (meow-normal-define-key
     '("0" . meow-expand-0)
     '("1" . meow-expand-1)
     '("2" . meow-expand-2)
     '("3" . meow-expand-3)
     '("4" . meow-expand-4)
     '("5" . meow-expand-5)
     '("6" . meow-expand-6)
     '("7" . meow-expand-7)
     '("8" . meow-expand-8)
     '("9" . meow-expand-9)
     '("-" . negative-argument)
     '(";" . meow-reverse)
     '("(" . meow-beginning-of-thing)
     '(")" . meow-end-of-thing)

     '("n" . meow-left)
     '("N" . meow-left-expand)
     '("e" . meow-next)
     '("E" . meow-next-expand)
     '("u" . meow-prev)
     '("U" . meow-prev-expand)
     '("i" . meow-right)
     '("I" . meow-right-expand)
     '("l" . meow-back-word)
     '("L" . meow-back-symbol)
     '("y" . meow-next-word)
     '("Y" . meow-next-symbol)
     '("h" . meow-join)
     '("." . meow-line)
     '("," . meow-mark-word)
     '("<" . meow-mark-symbol)
     ;; '("m" . eglot-prefix-map)

     '("b" . meow-inner-of-thing)
     '("v" . meow-bounds-of-thing)
     '("/" . meow-visit)
     '("r" . meow-insert)
     '("R" . meow-open-above)
     '("s" . meow-append)
     '("S" . meow-open-below)
     '("k" . meow-search)
     '("g" . meow-cancel-selection)
     '("G" . meow-grab)
     '("c" . meow-change)
     '("x" . meow-delete)
     '("d" . meow-kill)
     '("w" . meow-save)
     '("p" . meow-yank)
     '("q" . meow-quit)
     '("t" . meow-till)
     '("f" . meow-find)
     '("o" . meow-block)
     '("O" . meow-to-block)
     '("j" . meow-undo)
     '("J" . meow-undo-in-selection)
     '("z" . meow-pop-selection)
     '("a" . meow-replace)
     '("A" . meow-swap-grab)
     '("'" . repeat)
     '("<escape>" . ignore)))

  (defun meow-setup-default ()
    (setq meow-cheatsheet-layout meow-cheatsheet-layout-colemak)
    (meow-motion-overwrite-define-key
     ;; Use e to move up, n to move down.
     ;; Since special modes usually use n to move down, we only overwrite e here.
     '("e" . meow-prev)
     '("<escape>" . ignore))
    (meow-leader-define-key
     '("?" . meow-cheatsheet)
     ;; To execute the originally e in MOTION state, use SPC e.
     '("e" . "H-e")
     '("1" . meow-digit-argument)
     '("2" . meow-digit-argument)
     '("3" . meow-digit-argument)
     '("4" . meow-digit-argument)
     '("5" . meow-digit-argument)
     '("6" . meow-digit-argument)
     '("7" . meow-digit-argument)
     '("8" . meow-digit-argument)
     '("9" . meow-digit-argument)
     '("0" . meow-digit-argument))
    (meow-normal-define-key
     '("0" . meow-expand-0)
     '("1" . meow-expand-1)
     '("2" . meow-expand-2)
     '("3" . meow-expand-3)
     '("4" . meow-expand-4)
     '("5" . meow-expand-5)
     '("6" . meow-expand-6)
     '("7" . meow-expand-7)
     '("8" . meow-expand-8)
     '("9" . meow-expand-9)
     '("-" . negative-argument)
     '(";" . meow-reverse)
     '("," . meow-inner-of-thing)
     '("." . meow-bounds-of-thing)
     '("(" . meow-beginning-of-thing)
     '(")" . meow-end-of-thing)
     '("/" . meow-visit)
     '("a" . meow-append)
     '("A" . meow-open-below)
     '("b" . meow-back-word)
     '("B" . meow-back-symbol)
     '("c" . meow-change)
     '("x" . meow-delete)
     '("e" . meow-prev)
     '("E" . meow-prev-expand)
     '("f" . meow-find)
     '("g" . meow-cancel-selection)
     '("G" . meow-grab)
     '("m" . meow-left)
     '("M" . meow-left-expand)
     '("i" . meow-right)
     '("I" . meow-right-expand)
     '("j" . meow-join)
     '("k" . meow-kill)
     '("l" . meow-line)
     '("L" . meow-goto-line)
     '("w" . meow-mark-word)
     '("W" . meow-mark-symbol)
     '("n" . meow-next)
     '("N" . meow-next-expand)
     '("o" . meow-block)
     '("O" . meow-to-block)
     '("p" . meow-yank)
     '("q" . meow-quit)
     '("r" . meow-replace)
     '("R" . meow-swap-grab)
     '("s" . meow-insert)
     '("S" . meow-open-above)
     '("t" . meow-till)
     '("u" . meow-undo)
     '("U" . meow-undo-in-selection)
     '("v" . meow-search)
     '("h" . meow-next-word)
     '("H" . meow-next-symbol)
     '("d" . meow-kill)
     '("y" . meow-save)
     '("z" . meow-pop-selection)
     '("'" . repeat)
     '("<escape>" . ignore)))

  :hook (after-init . (lambda () (my/meow-setup) (meow-global-mode 1)))
  :config
  (setf (alist-get 'vterm-mode meow-mode-state-list) 'insert)
  (push '(sldb-mode . insert) meow-mode-state-list)
  (push '(notmuch-hello-mode . motion) meow-mode-state-list)
  (push '(notmuch-search-mode . motion) meow-mode-state-list)
  (push '(notmuch-show-mode . motion) meow-mode-state-list)
  (keymap-set meow-normal-state-keymap "m" eglot-prefix-map))

(use-package rainbow-delimiters
  :ensure t
  :pin nongnu
  :hook (prog-mode . rainbow-delimiters-mode-enable))

(use-package hl-line
  :hook prog-mode)

(use-package files
  :defer t
  :custom (save-abbrevs nil))

(use-package cc-mode
  :preface
  (defun setup-abbrevs ()
    (abbrev-mode 1)
    (define-abbrev local-abbrev-table "i8" "int8_t")
    (define-abbrev local-abbrev-table "i16" "int16_t")
    (define-abbrev local-abbrev-table "i32" "int32_t")
    (define-abbrev local-abbrev-table "i64" "int64_t")

    (define-abbrev local-abbrev-table "u8" "uint8_t")
    (define-abbrev local-abbrev-table "u16" "uint16_t")
    (define-abbrev local-abbrev-table "u32" "uint32_t")
    (define-abbrev local-abbrev-table "u64" "uint64_t"))

  :custom
  (c-basic-offset 4)
  (c-default-style "k&r")
  :bind (:map c-mode-map
              ("<tab>" . indent-for-tab-command))
  :hook (c-mode . setup-abbrevs))

(use-package conf-mode
  :bind (:map conf-mode-map
              ("<tab>" . indent-for-tab-command)))

(use-package simple
  :hook ((text-mode . auto-fill-mode)
         (html-mode . (lambda () (auto-fill-mode -1)))))

(use-package repeat
  :init (setq dired-jump-map nil)
  :config (repeat-mode 1))

(use-package autorevert
  :config (global-auto-revert-mode 1))

(use-package elec-pair
  :config (electric-pair-mode 1))

(use-package avy
  :ensure t
  :pin gnu
  :custom (avy-keys '(?a ?r ?s ?t ?g ?m ?n ?e ?i))
  :bind ("C-c l" . avy-goto-line))

(use-package notmuch
  :load-path "/usr/share/emacs/site-lisp/elpa-src/notmuch-0.37"
  :commands notmuch
  :custom
  (notmuch-show-logo nil)
  (notmuch-hello-auto-refresh t)
  (notmuch-search-oldest-first nil))

;; Random settings
(setq backup-directory-alist '(("." . "~/.emacs.d/backups"))
      inhibit-startup-screen t
      ring-bell-function #'ignore
      tab-always-indent 'complete
      vc-follow-symlinks t
      help-window-select t
      use-short-answers t
      dabbrev-check-other-buffers nil
      custom-file (make-temp-file "emacs-custom-")
      ;; Minibuffer
      enable-recursive-minibuffers t
      minibuffer-depth-indicate-mode 1
      minibuffer-electric-default-mode 1)

(setq-default indent-tabs-mode nil
              tab-width 4
              fill-column 80)

;; Swap some keybindings around so that they are less CRINGE
(global-set-key (kbd "C-h C-w") #'where-is)
(global-set-key (kbd "C-h w") #'describe-no-warranty)

(global-set-key (kbd "C-h C-f") #'describe-function)
(global-set-key (kbd "C-h f") #'view-emacs-FAQ)

(global-set-key (kbd "C-x C-b") #'switch-to-buffer)
(global-set-key (kbd "C-x b") #'buffer-menu)

(global-set-key (kbd "C-x C-o") #'other-window)
(global-set-key (kbd "C-x o") #'delete-blank-lines)

(global-set-key (kbd "C-x C-0") #'delete-window)
(global-set-key (kbd "C-x 0") #'text-scale-adjust)

(global-set-key (kbd "C-h C-m") #'describe-mode)
(global-set-key (kbd "C-h m") #'view-order-manuals)

(global-set-key (kbd "C-h C-a") #'apropos)
(global-set-key (kbd "C-h a") #'about-emacs)

;; For `vc-mode'
(global-unset-key (kbd "C-x C-v"))

;; For `project.el'
(global-unset-key (kbd "C-x C-p"))

(global-set-key (kbd "<f5>") #'compile)
(global-set-key (kbd "<f6>") #'recompile)

(define-key global-map (kbd "C-z") nil)
(define-key global-map (kbd "C-x C-r") nil)

(put 'upcase-region 'disabled nil)
