;; Resizing the emacs frame can be expensive part of changing font.
;; Inhibiting this can half startup time.
(setq frame-inhibit-implied-resize t)
(setq package-enable-at-startup nil)

;; Concatenate all autoloads files into one file
(setq-default package-quickstart t)
