#    _               _              
#   | |__   __ _ ___| |__  _ __ ___ 
#   | '_ \ / _` / __| '_ \| '__/ __|
#  _| |_) | (_| \__ \ | | | | | (__ 
# (_)_.__/ \__,_|___/_| |_|_|  \___|
                                  

export PATH=$PATH:~/.local/bin/zig
PS1='\[$(tput setaf 3)$(tput bold)\]\w\[$(tput sgr0)\] > '
export PS1

shopt -s histappend
history -a
shopt -s checkwinsize

# Epic functions for the lads
vterm_printf() {
    if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ]); then
        # Tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

vterm_prompt_end(){
    vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"
}

PS1=$PS1'\[$(vterm_prompt_end)\]'

if [[ "$INSIDE_EMACS" = 'vterm' ]]; then
    function clear() {
        vterm_printf "51;Evterm-clear-scrollback";
        tput clear;
    }
fi

j() {
    DIR=$(find ~ -type d -regex ".*/\.[^/]*$" -prune -o -type d -print | fzf --preview='ls {}')
    [ -d $DIR ] && cd $DIR
}

# Epic aliases for the boys and girls round the block
alias l='ls -XF --group-directories-first --color=auto'
alias ll='ls -AXFlh --group-directories-first --color=auto'
alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -Iv'
alias mkdir='mkdir -pv'
alias sed='sed -E'
alias grep='grep -Ei --color=auto'
alias v=nvim
alias vim=nvim
