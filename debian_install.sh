#!/bin/sh

sudo apt-get install bspwm build-essential cmake nitrogen pass suckless-tools git libtool-bin clangd cargo rustfmt rust-src rust-clippy
sudo apt-get install build-dep emacs
git config --global user.name "Luke McDougall"
git config --global user.email "l.a.mcdougall@protonmail.com"
