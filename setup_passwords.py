import subprocess
import sys

with open(sys.argv[1], "r") as f:
    passwords = f.read()
    for pair in passwords.split("\n\n"):
        name, _, password = pair.partition("\n")
        subprocess.run(["pass", "insert", "-e", "-f", name[:-4]], input=password.strip(), encoding='utf-8')
