#!/bin/sh

ID=$(bspc query -N -n any.local.window.hidden)
if [ $? -eq 0 ]; then
    bspc node $ID -g hidden
    bspc node $ID -f
else
    bspc node focused.local -g hidden
fi
