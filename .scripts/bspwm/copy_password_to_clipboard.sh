#!/bin/sh

cd "$HOME/.password-store"
PASSWORD=$(ls -A | dmenu -i -l 10)
[ -f "$PASSWORD" ] && pass -c ${PASSWORD%.gpg}
