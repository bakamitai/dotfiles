#!/bin/sh

direction=$1

case "$direction" in
    west) bspc node @west -r '-19' || bspc node @east -r '-19' ;;
    east) bspc node @west -r '+19' || bspc node @east -r '+19' ;;
    north) bspc node @south -r '-11' || bspc node @north -r '-11' ;;
    south) bspc node @south -r '+11' || bspc node @north -r '+11' ;;
esac
