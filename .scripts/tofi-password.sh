#!/bin/sh

cd "$HOME/.password-store"
PASSWORD=$(ls | tofi --font /usr/share/fonts/TTF/Hack-Regular.ttf --font-size=13   \
                     --prompt-text="password: " --horizontal=true --width=100%     \
                     --height=30 --anchor=top --ascii-input=true --outline-width=0 \
                     --border-width=0 --padding-top=0 --padding-bottom=0           \
                     --padding-left=0 --padding-right=0 --result-spacing=15        \
                     --min-input-width=120)
[ -f "$PASSWORD" ] && pass -c ${PASSWORD%.gpg}
